module.exports.formatUsername = (username) => {
	if (!username) {
		username = "Deleted User";
	}
	
	if (username.toLowerCase().endsWith("s")) {
		username += "'";
	} else {
		username += "'s";
	}
	
	return username;
};

module.exports.chunkMessage = (message, chunkSize, tag) => {
	const realTag = tag.replace("new", "same");
	const realChunkSize = chunkSize - realTag.length - 1;
	
	const regexp = new RegExp(`[^]{1,${realChunkSize}}`, "g");
	
	const realMessage = message.slice(tag.length);
	const chunks = realMessage.match(regexp);
	
	chunks[0] = `${tag}${chunks[0]}`
	for (let i = 1; i < chunks.length; i++) {
		chunks[i] = `${realTag} ${chunks[i]}`;
	}
	
	return chunks;
};
