#!/usr/bin/env node

require("dotenv").config({path: __dirname + "/keys.env"});

require("./discord.js");
require("./vk.js");
