const
	config = require("./config.json") || [],
	
	util = require("./util.js"),
	discord = require("./discord.js"),
	
	vk = require("vk-io");

const bot = new vk.VK({token: process.env.BAAKO_VK_TOKEN});

async function getUsername(userId) {
	try {
		const userInfo = await bot.api.users.get({
			user_ids: [userId],
			fields: ["domain"]
		});
		
		return userInfo[0].domain;
	} catch (error) {
		console.error(error);
		
		return null;
	}
}

module.exports.send = async (chatId, message) => {
	try {
		await bot.api.messages.send({
			chat_id: chatId,
			message: message,
			
			random_id: Date.now()
		});
	} catch (error) {
		console.error(error);
	}
};

bot.updates.on("message", async (message) => {
	if (!message.text) {
		return;
	}
	
	if (message.text == "/baako") {
		message.send(`${message.chatId}`);
		
		return;
	}
	
	const author = util.formatUsername(
		await getUsername(message.senderId)
	);
	
	for (const option of config) {
		for (const tag of option.tags) {
			if (
				message.text.length > tag.length &&
				message.text.toLowerCase().startsWith(tag)
			) {
				const chunkSize = 2000 - author.length - 1;
				const chunks = util.chunkMessage(message.text, chunkSize, tag);
				
				for (const chunk of chunks) {
					discord.send(option["channel-id"], `${author} ${chunk}`);
				}
				
				return;
			}
		}
	}
})

bot.updates.start().catch(console.error);

console.log("VK bot is ready");
