const
	config = require("./config.json") || [],
	
	util = require("./util.js"),
	otp = require("./otp.js"),
	vk = require("./vk.js"),
	
	discord = require("discord.js");

const bot = new discord.Client({
	intents: [
		discord.Intents.FLAGS.GUILDS,
		discord.Intents.FLAGS.GUILD_MESSAGES
	]
});

module.exports.send = async (channelId, message) => {
	try {
		await bot.channels.cache.get(channelId).send(message);
	} catch (error) {
		console.error(error);
	}
};

function runCommand(command, message) {
	const separator = command.indexOf(" ");
	
	let
		type = "",
		args = null;
	
	if (separator < 0) {
		type = command;
	} else {
		type = command.slice(0, separator);
		
		const rawArgs = command.slice(separator + 1);
		
		try {
			args = JSON.parse(rawArgs);
		} catch (_) {
			message.reply("This doesn't look like valid JSON.");
			
			return;
		}
	}
	
	let reason = "Idk.";
	
	switch (type) {
	case "help":
		message.reply("Only weaklings need help.");
		
		break;
	case "where-am-i": // FALLTHROUGH
	case "":
		message.reply(`${message.channel.id}`);
		
		break;
	case "who-am-i":
		message.reply(`${message.author.tag}`);
		
		break;
	case "check-password":
		reason = otp.isInvalid(args);
		
		if (reason) {
			message.reply(reason);
			
			return;
		}
		
		message.reply("Yes.");
		
		break;
	case "leave":
		reason = otp.isInvalid(args);
		
		if (reason) {
			message.reply(reason);
			
			return;
		}
		
		message.reply("Okay then.").then(() => {
			message.guild.leave();
		});
		
		break;
	default:
		message.reply("No such command. Enter `help` for help.");
		
		break;
	}
}

bot.on("ready", () => {
	console.log("Discord bot is ready");
});

bot.on("messageCreate", (message) => {
	if (!message?.content) {
		return;
	}
	
	if (message.content.startsWith("/baako")) {
		const command = message.content.slice(7);
		
		runCommand(command, message);
		
		return;
	}
	
	const author = util.formatUsername(message.member?.displayName);
	
	for (const option of config) {
		for (const tag of option.tags) {
			if (
				message.content.length > tag.length &&
				message.content.toLowerCase().startsWith(tag) &&
				(!option.private || message.member.roles.cache.size > 1)
			) {
				vk.send(option["chat-id"], `${author} ${message.content}`);
				
				return;
			}
		}
	}
});

bot.login(process.env.BAAKO_DISCORD_TOKEN);
