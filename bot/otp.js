const generate = require("totp-generator");

let
	lastAttempt = 0,
	lastPassword = "";

module.exports.isInvalid = function(password) {
	const currentTime = Date.now();
	
	let reason = false;
	
	if (currentTime - lastAttempt < 10 * 1000) {
		reason = "Whoa, not so fast.";
	}
	
	if (
		!reason &&
		password == lastPassword &&
		currentTime - lastAttempt < 30 * 1000
	) {
		reason = "Huh, this one looks the same as last one.";
	}
	
	if (
		!reason &&
		password != generate(process.env.BAAKO_OTP_SECRET)
	) {
		reason = "I don't think this is the right password.";
	}
	
	lastPassword = password;
	lastAttempt = currentTime;
	
	return reason;
};
